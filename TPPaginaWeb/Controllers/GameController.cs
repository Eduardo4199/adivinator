﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TPPaginaWeb.Filters;
using TPPaginaWeb.Models;

namespace TPPaginaWeb.Controllers
{
    public class GameController : Controller
    {
        private int number;/* Numero que ingresa el jugador y que se evalua*/
        public int Number { get => number; set => number = value; }
        private Juego juego = Juego.Instancia.GetJuego();
        public ActionResult EmpezarJuego()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public void ComprobarJugada(int num, Partida p)
        {
            int retorno = p.Jugar(num);
            if ( retorno == 0)
            {
                Jugador j = Session["Jugador"] as Jugador;
                p.Gano = true;
                j.PartidasGanadas = +1;
                j.PartidasGanadasSesion = +1;
            }
            else
            {
                if(retorno < 0)
                {
                    ViewBag.Pista = "El numero es mayor";
                }
                else
                {
                    ViewBag.Pista = "El numero es menor";
                }
            }
            Session["Partida"] = p;
        }

        /*private ActionResult Ganar()
        {
            Jugador j = Session["Jugador"] as Jugador;
            j.PartidasGanadas = j.PartidasGanadas + 1;
            j.PartidasGanadasSesion = j.PartidasGanadasSesion + 1;
            Session["Jugador"] = j;
            Session["Partida"] = null;
            return View();
        }*/
        
        //[AuthFilter]
        //[HttpPost]
        public ActionResult JugarJuego(string nombre, int? numero)
        {
            /*Acciones a realizar si hay un jugador en session*/
            SetPuntajeCookie(juego.Jugadores);
            if (Session["Jugador"]!= null)
            {
                /*Comprueba si hay una partida en curso, de lo contrario crea una*/
                if (Session["Partida"] == null)
                {
                    Jugador jug = Session["Jugador"] as Jugador;
                    Session["Partida"] = juego.IniciarPartida(jug);
                }
                /*Si hay una partida en progreso*/
                else
                {
                    Partida p = Session["Partida"] as Partida;
                    /*Si el numero ingresado no esta vacio*/
                    if (numero.HasValue)
                    { 
                        int num = numero.Value;
                        ComprobarJugada(num, p);
                        ViewBag.Numero = p.NumeroAleatorio;
                        /* Si la partida fue ganada*/
                        if (p.Gano == true)
                        {
                            Jugador j = Session["Jugador"] as Jugador;
                            j.PartidasGanadas = j.PartidasGanadas + 1;
                            j.PartidasGanadasSesion = j.PartidasGanadasSesion + 1;
                            Session["Jugador"] = j;
                            Session["Partida"] = null;
                            SetPuntajeCookie(juego.Jugadores);
                            return View("Ganar");
                        }
                        /*Si la partida ya acabo y no gano*/
                        else if (p.Perdio == true)
                        {
                            Session["Partida"] = null;
                            return View("Perder");
                        }
                    }
                }
            }
            else
            {
                /*Crear o obtener un jugador, y luego iniciar una partida */
                Session["Jugador"] = juego.ObtenerJugador(nombre);
                Session["Partida"] = juego.IniciarPartida(juego.ObtenerJugador(nombre));
            }
            /*Devuelve los intentos que el usuario haya ingresado*/
            Partida part = Session["Partida"] as Partida;
            ViewBag.IntentosHechos = part.Intentos;
            return View(part.Intentos);
        }
        [HttpPost]
        public ActionResult Abandonar()
            /*Guarda los datos del jugador y luego abandona la sesion*/
        {
            Jugador j = Session["Jugador"] as Jugador;
            j.PartidasGanadasSesion = 0;
            Juego jueg = Juego.Instancia;
            jueg.GuardarJugador(j.Nombre, j);
            SetPuntajeCookie(jueg.Jugadores);
            Session.Abandon();
            return Redirect("Index"); 
        }
        [AuthFilter]
        public ActionResult Ranking()
        {
            /*Muestra el ranking de todos los jugadores y el puntaje actual de la sesion*/
            Session["Partida"] = null;
            List<KeyValuePair<string, Jugador>> listaRanking = juego.Ranking(juego.Jugadores);
            ViewBag.Mejores = listaRanking;
            Jugador jug = Session["Jugador"] as Jugador;
            ViewBag.MejorMarcaSession = jug.PartidasGanadasSesion;
            ViewBag.MejorMarcaCookie = GetPuntajeCookie();
            return View();
        }
        public ActionResult Perder()
        {
            return View();
        }
        private void SetPuntajeCookie(Dictionary<string, Jugador> jugadores)
        {
            /*Guarda lista de jugadores en cookie*/
            JavaScriptSerializer js = new JavaScriptSerializer();
            string jug = js.Serialize(jugadores);
            HttpCookie puntuacion = new HttpCookie("Puntuacion");
            puntuacion.Expires = new DateTime(2019,09,12);
            puntuacion.Value = jug;
            System.Web.HttpContext currentCont = System.Web.HttpContext.Current;
            if(System.Web.HttpContext.Current.Request.Cookies["Puntuacion"] != null)
            {
                System.Web.HttpContext.Current.Response.Cookies.Remove("Puntuacion");
            }
            System.Web.HttpContext.Current.Response.Cookies.Add(puntuacion);
        }
        private Dictionary<string, Jugador> GetPuntajeCookie()
        {
            System.Web.HttpContext currentCont = System.Web.HttpContext.Current;
            if(System.Web.HttpContext.Current.Request.Cookies["Puntuacion"] == null)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var puntajes = System.Web.HttpContext.Current.Request.Cookies["Puntuacion"].Value;
                Dictionary<string, Jugador> jugadores = js.Deserialize <Dictionary<string, Jugador>>("Puntuacion");
                return jugadores;
            }
            else
            {
                return new Dictionary<string, Jugador>();
            }
        }
    }
}
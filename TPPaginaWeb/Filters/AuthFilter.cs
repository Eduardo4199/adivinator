﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TPPaginaWeb.Filters
{
    public class AuthFilter : ActionFilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Session["Jugador"] == null)
            {
                filterContext.Result = new RedirectToRouteResult( new RouteValueDictionary { {"Controller","Game"}, {"Action","Index" } });
            }
        }
    }
}
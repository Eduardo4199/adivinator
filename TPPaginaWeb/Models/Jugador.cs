﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPPaginaWeb.Models
{
    public class Jugador
    {
        private string nombre;
        public string Nombre { get => nombre; set => nombre = value; }
        private int partidasGanadasSesion=0;
        public int PartidasGanadasSesion { get => partidasGanadasSesion; set => partidasGanadasSesion = value; }
        private int partidasGanadas=0;
        public int PartidasGanadas { get => partidasGanadas; set => partidasGanadas = value; }
        public Jugador(string nombre)
        {
            Nombre = nombre;
        }
    }
}
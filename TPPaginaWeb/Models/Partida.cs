﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TPPaginaWeb.Models
{
    public class Partida
    {
        
        public Jugador Jugador { get; set; }
        private int intentosMax;
        public int IntentosMax { get => intentosMax; set => intentosMax = value; }
        private int numeroAleatorio;
        public int NumeroAleatorio { get => numeroAleatorio; set => numeroAleatorio = value; }
        private bool gano = false;
        public bool Gano { get => gano; set => gano = value; }
        private bool perdio = false;
        public bool Perdio { get => perdio; set => perdio = value; }
        private ArrayList intentos = new ArrayList();
        public ArrayList Intentos { get => intentos; }

        public int Jugar(int numero)
        {
            /* Valida si el numero ingresado por el usuario es el numero correcto */
            if(Validar(numero))
                if (!gano)
                {
                    intentos.Add(numero);
                }
                if (intentos.Count == 10)
                {
                    Perdio = true;
                }
                return numero - NumeroAleatorio;
        }

        private bool Validar(int numero)
        {
            /* Validar si el numero que ingreso el usuario es un numero valido */
            if (numero <= 100 && numero >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Partida(Jugador j, int intentosMax)
        {
            /*Crea una nueva partida*/
            Jugador = j;
            IntentosMax = intentosMax;
            Random numeroAl = new Random();
            NumeroAleatorio = numeroAl.Next(1,100);
        }

    }
}
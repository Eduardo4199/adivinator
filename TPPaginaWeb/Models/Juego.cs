﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace TPPaginaWeb.Models
{
    public class Juego
    {
        private Dictionary<string, Jugador> jugadores = new Dictionary<string, Jugador>();
        public Dictionary<string, Jugador> Jugadores { get => jugadores; }
        private static Juego instancia;
        public static Juego Instancia
            {
                get
                {
                    if(instancia == null)
                    {
                        instancia = new Juego();
                        return instancia;
                    }
                    else
                    {
                        return instancia;
                    }
                }
            }
        private Juego()
        {         
        }
        public Juego GetJuego()
        {
            return Instancia;
        }
        public void GuardarJugador(string nombre ,Jugador j)
        {
            jugadores.Remove(nombre);
            jugadores.Add(nombre,j);
        }
        public Jugador ObtenerJugador(string nombre)
        {
            if(!jugadores.ContainsKey(nombre))
            {
                Jugador j = new Jugador(nombre);
                Jugadores.Add(nombre, j);
                return j;
            }
            else
            {
                return jugadores[nombre];
            }
        }
        public Partida IniciarPartida(Jugador j)
        {
            return new Partida(j,10);
        }
        public List<KeyValuePair<string, Jugador>> Ranking(Dictionary<string,Jugador> jugadores)
        {
            var listaJugadores = jugadores.ToList();
            listaJugadores.Sort((item1,item2) => item1.Value.PartidasGanadas.CompareTo(item2.Value.PartidasGanadas));
            return listaJugadores;
        }
    }
}